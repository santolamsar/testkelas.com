<?php
function countCharacter($str) {
    $str = str_replace(' ', '', $str);
    $str = strtolower($str);
    $newStr = implode('', array_unique(str_split($str)));
    $count = array();
    for ($i = 0; $i < strlen($newStr); $i++) {
        $count[$newStr[$i]] = substr_count($str, $newStr[$i]);
    }
    $result = '';
    foreach ($count as $key => $value) {
        if ($value > 1) {
            $result .= $key . $value;
        } else {
            $result .= $key;
        }
    }
    echo $result;
}

countCharacter("aaaabsdfbhckkhfdkml");
?>